import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  title: string = 'My first AGM project';
  adicionar: boolean = false;

  map = {
  	lat: -15.834850,
  	lng: -48.020224,
  	minZoom: 10,
  	maxZoom: 18,
    zoom: 12
  }

  markertemp = {
    lat: -15.834840,
    lng: -48.020224,
    active:false
  }

  coordenadas: any;

  markers = [
  	{lat:51.678418,lng:7.809007,id:1},
  	{lat:-15.834850,lng:-48.020224,id:2}
  ];

  newgrafite = [];
  constructor() { } 

  ngOnInit() { }

  addGraffite(){
    if (!this.adicionar) {
      this.adicionar = true;
      this.map.minZoom = 18;
      console.log(this.map.minZoom);
      console.log('entrei');
    }
  }

  changeMapLocation(position){
  	console.log(position);
  	
  }

  mapClicked(evento){
    if (this.markertemp.active == false && this.adicionar) 
      this.markertemp.active = true;

  	if (this.adicionar) {
      this.markertemp.lat = evento.coords.lat;
      this.markertemp.lng = evento.coords.lng;
    }
  }

  closeAdd(evento){
  	if (typeof evento != "undefined"){
      evento.graffite.lat = this.markertemp.lat;
      evento.graffite.lng = this.markertemp.lng;
      this.newgrafite = evento.graffite;this.markertemp.lat;
      console.log(this.newgrafite);
    }
  	else{
      this.adicionar = false;
      this.markertemp.active = false;
      this.map.minZoom = 10;
    }
  }
  getId(evento){
    console.log(evento);
  }

}
