import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-adicionargraffite',
  templateUrl: './adicionargraffite.component.html',
  styleUrls: ['./adicionargraffite.component.css']
})
export class AdicionargraffiteComponent implements OnInit {
	@Output() emitter: EventEmitter<any[]> = new EventEmitter();

  graffite = {
    nome:"",
    autor:"",
    ispublic:undefined,
    addimg:false,
    file:undefined,
    lat:undefined,
    lng:undefined
  };

  constructor() { }
        
  ngOnInit() {  }

  fileChange(event) {
    let img;
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {      
      let file: File = fileList[0];
       img = document.querySelector(".adicionar img");
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
      img.src = reader.result;
      }, false);
      if (file) {
        this.graffite.addimg = true;
        this.graffite.file = file;
        reader.readAsDataURL(file);
      }    
    }
  }

  fecharCard(){  
  	this.emitter.emit();
  }
  salvar(){

    if (this.graffite.nome != '' && this.graffite.autor != '' && this.graffite.ispublic && this.graffite.addimg) {
      let salvarGraffite:any = {salvar: true,graffite:this.graffite};
      this.emitter.emit(salvarGraffite);
    }
    else{
      console.log("TEM COISA FALTANDO");
    }
   

  }

  setNome(nome){
    this.graffite.nome = nome;
  }

  setAutor(nome){
    this.graffite.autor = nome;
  }

  setLocal(nome){
    if (nome != "publico") {
      this.graffite.ispublic = false;
    }
    else{
      this.graffite.ispublic = true;
      
  }
    }
}
