import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionargraffiteComponent } from './adicionargraffite.component';

describe('AdicionargraffiteComponent', () => {
  let component: AdicionargraffiteComponent;
  let fixture: ComponentFixture<AdicionargraffiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionargraffiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionargraffiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
