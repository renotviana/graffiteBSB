import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MapsComponent } from './maps/maps.component';
import { HomeComponent } from './home/home.component';
import { AdicionargraffiteComponent } from './adicionargraffite/adicionargraffite.component';

const APP_ROUTES: Routes = [
	{ path: '', component: MapsComponent },
	{ path: 'home', component: HomeComponent },
	{ path: 'login', component:  LoginComponent},
	{ path: 'add', component:  AdicionargraffiteComponent}
];
export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);