import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterializeModule } from 'angular2-materialize';

import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { MapsComponent } from './maps/maps.component';

import { AgmCoreModule } from '@agm/core';

import { AdicionargraffiteComponent } from './adicionargraffite/adicionargraffite.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    MapsComponent,
AdicionargraffiteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MaterializeModule,
    routing,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCzRWKANl3XVlsJICbysV6pAViK16ujB8w'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
